# git_springboot_restservice

#### 介绍
官网代码，搭建REST风格的Service，可以利用SpringToolsSuite、InteliJ等IDE环境开发源码，Java环境为Jdk1.8

#### 软件架构
软件架构说明
使用SpringBoot微服务架构来搭建WebService服务


#### 安装教程

1. 系统安装：安装Windows 64/32，Linux 64/32
2. JDK1.8安装：注意和系统对应，系统如果是64位的，jdk最好安装64位的；Linux如果自带jdk，最好卸载重新安装。
                   java -version； //正常显示安装版本号和位数
                   java;                 //java环境正常
                   javac;               //java编译环境正常
    如果是绿色安装，系统环境变量需要增加JAVA_HOME项。
3. 建议安装Spring官方的IDE工具https://spring.io/tools；或者IntelliJ IDEA_Ultimate版，需要支持SpringBoot。
   可以New->Project时能否建立SpringBoot Project来确定是否支持SpringBoot，建议下载最新的，现在Spring核心是5.0版本，SpringBoot是2.0版 
   本。

#### 使用说明

1. 通过IDE环境下Import工程的方式，加载目录下对应工程来编译、运行、测试
     Import选择Maven工程；
     编译需要下载Jar包，请等待；
     运行使用SpringBoot；
2. 测试是否正常：
     打开浏览器：http://127.0.0.1:8080/greeting  ；结果显示  {"id":1,"content":"Hello, World!"}
     测试参数：   http://127.0.0.1:8080/greeting?name=avens   ；结果显示 {"id":2,"content":"Hello, avens!"}
此工程为RestFull Service，返回Json

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)